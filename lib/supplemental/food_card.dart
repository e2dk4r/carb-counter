import 'package:flutter/material.dart';

import '../model/food.dart';

class FoodCard extends StatelessWidget {
  final Food food;

  FoodCard({@required this.food});

  Widget _bubbleText(String key, double value) {
    return Container(
      child: Column(
        children: <Widget>[
          Text(key),
          SizedBox(height: 8.0),
          Text(value.toString()),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    if (food == null) {
      return Text('Loading');
    }

    return Column(
      children: <Widget>[
        // Name of the food
        Text('Name: ${food.name}'),
        SizedBox(height: 10.0),
        FoodValuesCard(food: food)
      ],
    );
  }
}

class FoodValuesCard extends StatelessWidget {
  final Food food;

  FoodValuesCard({@required this.food});

  Widget _bubbleText(String key, double value) {
    return Container(
      child: Column(
        children: <Widget>[
          Text(key),
          SizedBox(height: 8.0),
          Text(value.toString()),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Text(
        'Nutritional Values (100gr)',
        textAlign: TextAlign.center,
        style: TextStyle(
            fontWeight: FontWeight.bold, color: Colors.red, fontSize: 18),
      ),
      // Nutrion Facts
      SizedBox(height: 10.0),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _bubbleText('Calories', food.nutritionFacts.calorie),
          SizedBox(width: 15.0),
          _bubbleText('Carbs', food.nutritionFacts.carbs),
          SizedBox(width: 15.0),
          _bubbleText('Fat', food.nutritionFacts.fat),
          SizedBox(width: 15.0),
          _bubbleText('Protein', food.nutritionFacts.protein),
        ],
      ),
      Text(
        'Exchage List Values (100gr)',
        textAlign: TextAlign.center,
        style: TextStyle(
            fontWeight: FontWeight.bold, color: Colors.red, fontSize: 18),
      ),
      SizedBox(height: 10.0),
      // Exchange List
      SizedBox(height: 10.0),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _bubbleText('Vegetable', food.exchangeList.vegetable),
          SizedBox(width: 15.0),
          _bubbleText('Fruit', food.exchangeList.fruit),
          SizedBox(width: 15.0),
          _bubbleText('Fat', food.exchangeList.fat),
          SizedBox(width: 15.0),
          _bubbleText('Milk', food.exchangeList.milk),
          SizedBox(width: 15.0),
          _bubbleText('Meat', food.exchangeList.meat),
          SizedBox(width: 15.0),
          _bubbleText('Starch', food.exchangeList.starch),
        ],
      ),
    ]);
  }
}
