import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math' as Math;

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:scoped_model/scoped_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image/image.dart' as Img;
import 'package:path/path.dart';

import '../model/app_state_model.dart';
import '../model/api_repository.dart';
import '../model/food.dart';
import '../supplemental/food_card.dart';

class PageProduce extends StatefulWidget {
  @override
  _PageProduceState createState() => _PageProduceState();
}

class _PageProduceState extends State<PageProduce> {
  File _imageFile;
  Food _food;

  var inputText = new TextEditingController();
  void _onImageButtonPressed() async {
    var imageFile = await ImagePicker.pickImage(source: ImageSource.camera);

    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path;

    int rand = new Math.Random().nextInt(100000);

    Img.Image image = Img.decodeImage(imageFile.readAsBytesSync());
    Img.Image smallerImg = Img.copyResize(image, height: 1280, width: 720);
    var compressImg = new File("$path/image_$rand.jpg")
      ..writeAsBytesSync(Img.encodeJpg(smallerImg, quality: 85));

    setState(() {
      _imageFile = compressImg;
    });

    if (_imageFile != null) {
      await ApiRepository.upload(_imageFile);
    }
  }

  Widget _bubbleText(String key, double value) {
    return Container(
      child: Column(
        children: <Widget>[
          Text(
            key,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          ),
          SizedBox(height: 8.0),
          Text(value.toString()),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(8.0),
      children: <Widget>[
        Center(
          child: _imageFile == null
              ? Text('No image taken.')
              : Image.file(_imageFile),
        ),
        Container(
          width: double.infinity,
          child: RaisedButton.icon(
            onPressed: _onImageButtonPressed,
            icon: Icon(Icons.camera),
            label: Text('Take A Picture'),
          ),
        ),
        Container(
          width: double.infinity,
          child: RaisedButton(
            onPressed: () async {
              // TODO: Implement Google Vision
              List<String> labels =
                  await ApiRepository.fetchVision(new http.Client());
              print(labels.length);
              setState(() {
                for (var label in labels) {
                  for (var food
                      in ScopedModel.of<AppStateModel>(context).allFoods) {
                    print(label +
                        " =??= " +
                        food.name +
                        " => " +
                        food.name.contains(label).toString());
                    bool isFound = food.name.contains(label);
                    if (isFound != null && isFound) {
                      _food = food;
                      break;
                    }
                  }
                }

                if (_food != null) {
                  inputText.text = _food.name;
                }
              });
            },
            child: Text('Recognize'),
          ),
        ),
        SizedBox(height: 8.0),
        Divider(color: Colors.black),
        TextField(
          onChanged: (text) {
            // it is required that at least three letters be typed before the search function is activated.
            if (text.length <= 3) return;

            setState(() {
              _food =
                  ScopedModel.of<AppStateModel>(context, rebuildOnChange: true)
                      .allFoods
                      .firstWhere((f) => f.name.startsWith(text));
            });

            if (_food != null) {
              inputText.text = _food.name;
            }
          },
          controller: inputText,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: 'Recognized food\'s name',
          ),
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
        (_food != null)
            ? Center(child: Text('Category: ' + _food.category.name))
            : Text('No food category found'),
        (_food != null) ? FoodValuesCard(food: _food) : Text('No food found'),
        Divider(color: Colors.black),
        SizedBox(height: 8.0),
        RaisedButton(
          child: Text('Add to list'),
          onPressed: () {
            int foodId = _food.id;
            ScopedModel.of<AppStateModel>(context).addFoodToList(foodId);

            /* Reset to first state */
            setState(() {
              _imageFile = null;
              _food = null;
            });
          },
        )
      ],
    );
  }
}
