import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../supplemental/food_card.dart';
import '../model/app_state_model.dart';
import '../model/food.dart';

class PageList extends StatefulWidget {
  @override
  _PageListState createState() => _PageListState();
}

class _PageListState extends State<PageList> {
  List<Widget> _createFoodsList(AppStateModel model) {
    return model.foodsInList
        .map((int id) => FoodRow(
              food: model.getFoodById(id),
              onRemove: () {
                model.removeFoodFromList(id);
              },
            ))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ScopedModelDescendant<AppStateModel>(
          builder: (BuildContext context, Widget child, AppStateModel model) {
        return ListView(
          children: <Widget>[
            FoodListSummary(model: model),
            Column(children: _createFoodsList(model)),
          ],
        );
      }),
    );
  }
}

class FoodListSummary extends StatelessWidget {
  final AppStateModel model;

  const FoodListSummary({this.model});

  Widget _bubbleText(String key, double value) {
    return Container(
      child: Column(
        children: <Widget>[
          Text(key, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
          SizedBox(height: 8.0),
          Text(value.toString()),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text('Total Nutritional Values', textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red, fontSize: 18),),
        SizedBox(height: 10.0),
        // Total Nutrion Facts
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _bubbleText('Calories', model.totalNutritionFacts.calorie),
            SizedBox(width: 15.0),
            _bubbleText('Carbs', model.totalNutritionFacts.carbs),
            SizedBox(width: 15.0),
            _bubbleText('Fat', model.totalNutritionFacts.fat),
            SizedBox(width: 15.0),
            _bubbleText('Protein', model.totalNutritionFacts.protein),
          ],
        ),
        Text('Total Exchage List Values', textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red, fontSize: 18),),
        SizedBox(height: 10.0),
        // Total Exchange List
        SizedBox(height: 10.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _bubbleText('Vegetable', model.totalExchangeList.vegetable),
            SizedBox(width: 15.0),
            _bubbleText('Fruit', model.totalExchangeList.fruit),
            SizedBox(width: 15.0),
            _bubbleText('Fat', model.totalExchangeList.fat),
            SizedBox(width: 15.0),
            _bubbleText('Milk', model.totalExchangeList.milk),
            SizedBox(width: 15.0),
            _bubbleText('Meat', model.totalExchangeList.meat),
            SizedBox(width: 15.0),
            _bubbleText('Starch', model.totalExchangeList.starch),
          ],
        ),
        Divider(color: Colors.black),
        SizedBox(height: 8.0),
      ],
    );
  }
}

class FoodRow extends StatelessWidget {
  final Food food;
  final VoidCallback onRemove;

  const FoodRow({@required this.food, this.onRemove});

  @override
  Widget build(BuildContext context) {
    return Row(
      key: ValueKey<int>(food.id),
      children: <Widget>[
        Expanded(
          child: FlatButton(
            child: Text('${food.name}'),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FoodDetail(food: food)));
            },
          ),
        ),
        IconButton(icon: Icon(Icons.remove_circle_outline), onPressed: onRemove)
      ],
    );
  }
}

class FoodDetail extends StatelessWidget {
  final Food food;

  const FoodDetail({@required this.food});

  Widget _bubbleText(String key, double value) {
    return Container(
      child: Column(
        children: <Widget>[
          Text(key),
          SizedBox(height: 8.0),
          Text(value.toString()),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Food Detail"),
      ),
      body: FoodCard(food: food)
    );
  }
}
