import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http;
import 'package:barcode_scan/barcode_scan.dart';
import 'dart:async';
import 'package:flutter/services.dart';

import '../model/app_state_model.dart';
import '../model/api_repository.dart';
import '../model/food.dart';
import '../supplemental/food_card.dart';

class PageBarcode extends StatefulWidget {
  @override
  _PageBarcodeState createState() => _PageBarcodeState();
}

class _PageBarcodeState extends State<PageBarcode> {
  Food food;
  String message = "Food not found";
  String barcode = "";

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: double.infinity,
          child: RaisedButton(
            onPressed: _scanBarcode,
            child: Text('Scan barcode'),
          ),
        ),
        Text(
          "Barcode: " + barcode,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        ),
        SizedBox(height: 8.0),
        Divider(color: Colors.black),
        (food != null) ? FoodCard(food: food) : Text(message),
        Divider(color: Colors.black),
        SizedBox(height: 8.0),
        RaisedButton(
          child: Text('Add to list'),
          onPressed: _addToList,
        )
      ],
    );
  }

  Future _scanBarcode() async {
    try {
      String barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          message = 'No camera permission!';
        });
      } else {
        setState(() => message = 'Unknown error: $e');
      }
    } on FormatException {
      setState(() => message = 'Nothing captured.');
    } catch (e) {
      setState(() => message = 'Unknown error: $e');
    }

    setState(() {
      food = ScopedModel.of<AppStateModel>(context)
          .allFoods
          .singleWhere((food) => food.barcode == this.barcode);
    });
  }

  void _addToList() async {
    int foodId = food.id;
    ScopedModel.of<AppStateModel>(context).addFoodToList(foodId);

    /* Reset to first state */
    setState(() {
      message = "Food not found";
      food = null;
    });
  }
}
