import 'package:carb_counter/supplemental/food_card.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../model/app_state_model.dart';
import '../model/food.dart';

class PageMealSearch extends StatefulWidget {
  @override
  _PageMealSearchState createState() => _PageMealSearchState();
}

class _PageMealSearchState extends State<PageMealSearch> {
  Food food;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        TextField(
            decoration: InputDecoration(
              filled: true,
              labelText: 'Food name',
            ),
            onChanged: _textChanged),
        SizedBox(height: 16),
        FoodCard(food: food),
        SizedBox(height: 16),
        new ScopedModelDescendant<AppStateModel>(
          builder: (BuildContext context, Widget child, AppStateModel model) {
            return Container(
              width: double.infinity,
              child: RaisedButton(
                onPressed: () async {
                  print(model.foodsInList.length);
                  model.addFoodToList(food.id);

                  /* Reset to first state */
                  setState(() {
                    food = null;
                  });
                },
                child: child,
              ),
            );
          },
          child: Text('Add to List'),
        ),
      ],
    );
  }

  void _textChanged(String text) {
    // it is required that at least three letters be typed before the search function is activated.
    if (text.length <= 3) return;

    setState(() {
      food = ScopedModel.of<AppStateModel>(context, rebuildOnChange: true)
          .allFoods
          .firstWhere((f) => f.name.startsWith(text));
    });
  }
}
