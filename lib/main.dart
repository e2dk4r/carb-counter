import 'package:scoped_model/scoped_model.dart';
import 'package:flutter/material.dart';

import 'model/app_state_model.dart';
import 'page/page_barcode.dart';
import 'page/page_produce.dart';
import 'page/page_list.dart';
import 'page/page_mealSearch.dart';

void main() => runApp(CarbCounterApp());

class CarbCounterApp extends StatefulWidget {
  @override
  _CarbCounterAppState createState() => _CarbCounterAppState();
}

class _CarbCounterAppState extends State<CarbCounterApp> {
  AppStateModel model;

  @override
  void initState() {
    super.initState();
    model = AppStateModel()..loadFoods();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModel<AppStateModel>(
      model: model,
      child: MaterialApp(
        home: DefaultTabController(
          length: 4,
          child: Scaffold(
            appBar: AppBar(
              bottom: TabBar(
                tabs: [
                  Tab(
                    icon: Icon(Icons.camera_alt),
                    text: 'Barcode',
                  ),
                  Tab(
                    icon: Icon(Icons.fastfood),
                    text: 'Produce',
                  ),
                  Tab(
                    icon: Icon(Icons.search),
                    text: 'Meal Search',
                  ),
                  Tab(
                    icon: Icon(Icons.list),
                    text: 'List',
                  ),
                ],
              ),
              title: Text('Carb Counter'),
            ),
            body: TabBarView(
              children: [
                PageBarcode(),
                PageProduce(),
                PageMealSearch(),
                PageList(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
