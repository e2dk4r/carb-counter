import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

import '../model/category.dart';

class Food {
  const Food(
      {@required this.id,
      @required this.name,
      @required this.category,
      this.barcode,
      this.nutritionFacts,
      this.exchangeList})
      : assert(name != null),
        assert(category != null);

  final int id;
  final String name;
  final Category category;
  final String barcode;
  final NutritionFacts nutritionFacts;
  final ExchangeList exchangeList;

  factory Food.fromJson(Map<String, dynamic> json) {
    return Food(
      id: json['id'] as int,
      name: json['name'] as String,
      barcode: json['barcode']['code'] as String,
      category: Category.fromJson(json['category']),
      nutritionFacts: NutritionFacts.fromJson(json['nutrition_facts']),
      exchangeList: ExchangeList.fromJson(json['exchange_list']),
    );
  }
}

class NutritionFacts {
  final double calorie;
  final double carbs;
  final double fat;
  final double protein;

  NutritionFacts({this.calorie, this.carbs, this.fat, this.protein});

  factory NutritionFacts.fromJson(Map<String, dynamic> json) {
    num jsonCalorie = json['calorie'] as num;
    num jsonCarbs = json['carbs'] as num;
    num jsonFat = json['fat'] as num;
    num jsonProtein = json['protein'] as num;
    return NutritionFacts(
      calorie: jsonCalorie.toDouble(),
      carbs: jsonCarbs.toDouble(),
      fat: jsonFat.toDouble(),
      protein: jsonProtein.toDouble(),
    );
  }
}

class ExchangeList {
  final double vegetable;
  final double fruit;
  final double fat;
  final double milk;
  final double meat;
  final double starch;

  ExchangeList(
      {this.vegetable,
      this.fruit,
      this.fat,
      this.milk,
      this.meat,
      this.starch});

  factory ExchangeList.fromJson(Map<String, dynamic> json) {
    num jsonVegetable = json['vegetable'] as num;
    num jsonFruit = json['fruit'] as num;
    num jsonFat = json['fat'] as num;
    num jsonMilk = json['milk'] as num;
    num jsonMeat = json['meat'] as num;
    num jsonStarch = json['starch'] as num;
    return ExchangeList(
      vegetable: jsonVegetable.toDouble(),
      fruit: jsonFruit.toDouble(),
      fat: jsonFat.toDouble(),
      milk: jsonMilk.toDouble(),
      meat: jsonMeat.toDouble(),
      starch: jsonStarch.toDouble(),
    );
  }
}
