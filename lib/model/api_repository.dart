import 'dart:convert';
import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:async/async.dart';

import 'category.dart';
import 'food.dart';

class ApiRepository {
  static const String _urlPrefix = "##SERVER##/api/";
  static const String _urlFoods = "${_urlPrefix}foods";
  static const String _urlBarcodePrefix = "${_urlPrefix}foods/barcodes/";
  static const String _urlVision = _urlPrefix + 'vision';

  static List<Food> parseFoods(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

    return parsed.map<Food>((json) => Food.fromJson(json)).toList();
  }

  static Future<List<Food>> fetchFoods(http.Client client) async {
    final response = await client.get(_urlFoods);

    return compute(parseFoods, response.body);
  }

  static Food parseFood(String responseBody) {
    final parsed = json.decode(responseBody);

    return Food.fromJson(parsed);
  }

  static Future<Food> fetchFoodByBarcode(http.Client client, String barcode) async {
    final response = await client.get(_urlBarcodePrefix + barcode);

    if (response.statusCode != 200) {
      throw Exception('Failed to load food');
    }

    return compute(parseFood, response.body);
  }

  static Future<int> upload(File imageFile) async {
    var stream =
        new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
    var length = await imageFile.length();
    var uri = Uri.parse(_urlPrefix + 'upload');

    var request = new http.MultipartRequest("POST", uri);

    var multipartFile = new http.MultipartFile("photo", stream, length,
        filename: basename(imageFile.path));
    request.files.add(multipartFile);

    var response = await request.send();

    return response.statusCode;
  }

  static List<String> parseVision(String responseBody) {
    final parsed = json.decode(responseBody).cast<String>();

    return parsed.toList();
  }

  static Future<List<String>> fetchVision(http.Client client) async {
    final response = await client.get(_urlVision);

    return compute(parseVision, response.body);
  }
}
