import 'package:meta/meta.dart';

class Category {
  final int id;
  final String name;

  const Category({@required this.id, @required this.name})
      : assert(id != null),
        assert(name != null);

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
      id: json['id'] as int,
      name: json['name'] as String
    );
  }
}
