import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http;

import 'api_repository.dart';

import 'food.dart';

class AppStateModel extends Model {
  List<Food> _allFoods;
  List<int> _foodsInList = <int>[];

  List<Food> get allFoods => _allFoods;
  List<int> get foodsInList => _foodsInList;

  NutritionFacts get totalNutritionFacts {
    double totalCalories = _foodsInList
        .map((int id) => _allFoods.singleWhere((food) => food.id == id).nutritionFacts.calorie)
        .fold(0.0, (double sum, double e) => sum + e);
    
    double totalCarbs = _foodsInList
        .map((int id) => _allFoods.singleWhere((food) => food.id == id).nutritionFacts.carbs)
        .fold(0.0, (double sum, double e) => sum + e);

    double totalFats = _foodsInList
        .map((int id) => _allFoods.singleWhere((food) => food.id == id).nutritionFacts.fat)
        .fold(0.0, (double sum, double e) => sum + e);

    double totalProteins = _foodsInList
        .map((int id) => _allFoods.singleWhere((food) => food.id == id).nutritionFacts.protein)
        .fold(0.0, (double sum, double e) => sum + e);

    return NutritionFacts(
        calorie: totalCalories,
        carbs: totalCarbs,
        fat: totalFats,
        protein: totalProteins);
  }

  ExchangeList get totalExchangeList {
    double totalVegetables = _foodsInList
        .map((int id) => _allFoods.singleWhere((food) => food.id == id).exchangeList.vegetable)
        .fold(0.0, (double sum, double e) => sum + e);

    double totalFruits = _foodsInList
        .map((int id) => _allFoods.singleWhere((food) => food.id == id).exchangeList.fruit)
        .fold(0.0, (double sum, double e) => sum + e);

    double totalFats = _foodsInList
        .map((int id) => _allFoods.singleWhere((food) => food.id == id).exchangeList.fat)
        .fold(0.0, (double sum, double e) => sum + e);

    double totalMilks = _foodsInList
        .map((int id) => _allFoods.singleWhere((food) => food.id == id).exchangeList.milk)
        .fold(0.0, (double sum, double e) => sum + e);

    double totalMeats = _foodsInList
        .map((int id) => _allFoods.singleWhere((food) => food.id == id).exchangeList.meat)
        .fold(0.0, (double sum, double e) => sum + e);

    double totalStarch = _foodsInList
        .map((int id) => _allFoods.singleWhere((food) => food.id == id).exchangeList.starch)
        .fold(0.0, (double sum, double e) => sum + e);

    return ExchangeList(
      vegetable: totalVegetables,
      fruit: totalFruits,
      fat: totalFats,
      milk: totalMilks,
      meat: totalMeats,
      starch: totalStarch,
    );
  }

  Food getFoodById(int foodId) {
    return _allFoods.firstWhere((food) => food.id == foodId);
  }

  void addFoodToList(int foodId) {
    // if it is in list
    if (!_foodsInList.contains(foodId)) {
      _foodsInList.add(foodId);
      notifyListeners();
    }
  }

  void removeFoodFromList(int foodId) {
    if (_foodsInList.contains(foodId)) {
      _foodsInList.remove(foodId);
      notifyListeners();
    }
  }

  void loadFoods() async {
    _allFoods = await ApiRepository.fetchFoods(new http.Client());
    notifyListeners();
  }
}
